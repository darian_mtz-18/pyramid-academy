import java.util.Scanner;
import java.util.Random;

class HangMan {
    static Scanner input;
    public static void hangman() throws Exception{
        // Line 8 allows the user to input or interact with the game
        input = new Scanner(System.in);
        // line 10 is a hand made array of words
        String[] company = { "cat", "java", "pizza", "halo", "toyota" };
        // print statement saying welcome
        System.out.println("Welcome to HANGMAN GAME");
        // a randomized object
        Random obj = new Random();
        // number grabbing line 14 and putting it on the next line
        int Ran_num = obj.nextInt(5);
        // setting the string word to company connecting it with Ran_num on line 16
        String word = (company[Ran_num]);
        // Upper cases all the words
        word = word.toUpperCase();
        // line 22 goes through a set array of the alphabet, any time a letter is picked
        String word1 = word.replaceAll("[A-Z]", "_ ");
        // another print statement
        System.out.println("let's play the game");
        // this uses the functionality below, on line 28 while using the function above
        startGame(word, word1);
    }
    public static void startGame(String word, String word1) throws Exception{
        // integer set to 0
        int guess_ = 0;
        //integer set to 0
        int wrong = 0;
        // turn integer 0 or guess to a string
        String guess;
        // empty character called letter
        char letter;
        // true or false from what you guessed, but it's still empty, FOR NOW
        boolean guessescontainsguess;
        //guesses set to empty string
        String guesses = "";
        // true or false from what you guessed, but it's still empty, FOR NOW
        boolean guessinword;

        // 0 is greater than 5 and string word contains a "_", we are going to loop through the list
        while (wrong < 5 && word1.contains("_")) {

            //prints the alphabet array on the next line to see your options
            System.out.println(word1 + "\n");
            // integer temp is being set to 5 - 0
            int temp = 5 - wrong;
            //if wrong is not 0
            if (wrong != 0) {

                //print you have number of turns left
                System.out.println("You have " + temp + " turns left.");
            }
            //prints your guess
            System.out.print("Your Guess:");
            //allows the user to put another choice if they got it wrong
            guess = input.nextLine();
            // if write the upper case the chosen character
            guess = guess.toUpperCase();
            // Returns the char value at the specified index. An index ranges from 0 to length()
            letter = guess.charAt(0);
            guessescontainsguess = (guesses.indexOf(letter)) != -1;

            guesses += letter;

            letter = Character.toUpperCase(letter);
            System.out.println();

            if (guessescontainsguess == true) {

                System.out.println("You ALREADY guessed " + letter + ". \n");
            }
            guessinword = (word.indexOf(letter)) != -1;

            if (guessinword == true) {

                System.out.println(
                        letter + " is present in the word.");
                System.out.print("\n");

                for (int position = 0;
                     position < word.length(); position++) {

                    if (word.charAt(position) == letter
                            && word1.charAt(position)
                            != letter) {

                        word1 = word1.replaceAll("_ ", "_");
                        String word2;
                        word2 = word1.substring(0, position)
                                + letter
                                + word1.substring(position
                                + 1);
                        word2 = word2.replaceAll("_", "_ ");
                        word1 = word2;
                    }
                }
            }
            else {
                System.out.println(letter + " is not present in the word.");
                wrong++;
            }
            guess_++;
        }
        if (wrong == 5) {
            System.out.println("YOU LOST!, maximum limit of incorrect guesses reached.");
        }
        else {
            System.out.print("The word is: " + word1 + "\n Well Played, you did it!!");
        }
    }
    public static void main(String[] args) throws Exception {
        hangman();
    }
}

//Major reference
//https://www.geeksforgeeks.org/hangman-game-in-java/
import java.util.Random;
import java.util.Scanner;

public class HumanGoblin {

    private static Human human = new Human();

    private static Goblins goblin = new Goblins();

    public static void displayGrid(char [] [] newGame){
        for(char [] c : newGame){
            for(char cc : c){
                System.out.print(cc);
            }
            System.out.println();
        }
    }

    public static void setHumanGoblin(char [] [] drawGrid){
        Random rand = new Random();
        drawGrid [rand.nextInt(6)][rand.nextInt(6)] = human.humanIcon;
        drawGrid [rand.nextInt(6)][rand.nextInt(6)] = goblin.goblinIcon;
    }

    public static void main(String args[]) throws Exception{
        char [] [] newGame = Land.drawGrid();

        new HumanGoblin().setHumanGoblin(newGame) ;

        displayGrid(newGame);

        Scanner scan = new Scanner(System.in);
        String str = "";
        System.out.println("Welcome Player");
        System.out.println("to move use w/a/s/d");


        do{
            System.out.println("to quit tap q");
            str = scan.nextLine();
            human.move(newGame, str);
            goblin.move(newGame);
            displayGrid(newGame);


        }while(!(str.equalsIgnoreCase("q")));
    }
    //Everything must be objects: land/goblins/humans
    //	•	You must override the toString method to represent each of the object
    //	•	Create a grid for the game world
    //	•	Use UTF characters for the players and goblins and the land
    //	•	Game is turn based move: n/s/e/w
    //	•	Once a human and goblin collide combat is initiated
    //	•	Combat uses math.random
}



class Land {
    public static char [] [] drawGrid() throws Exception{
        char [] [] gameGrid = {
                {' ', '|', ' ', '|', ' ', '|', ' ', '|', ' ',},
                {' ', '|', ' ', '|', ' ', '|', ' ', '|', ' ',},
                {' ', '|', ' ', '|', ' ', '|', ' ', '|', ' ',},
                {' ', '|', ' ', '|', ' ', '|', ' ', '|', ' ',},
                {' ', '|', ' ', '|', ' ', '|', ' ', '|', ' ',},
                {' ', '|', ' ', '|', ' ', '|', ' ', '|', ' ',}
        };
        return gameGrid;
    }
}

class Human{

    char humanIcon = '\u4000';
    int humanStrength = 10;
    int humanHealth = 10;

    public char getHumans() {
        return humanIcon;
    }

    public void move(char[][] newGame, String str) {
        switch (str){
            case "Q": case "q":
                System.out.println("Thanks for playing Noob!");
                break;
            case "W": case "w":
                moveUp(newGame);
                break;
            case "A": case "a":
                moveLeft(newGame);
                break;
            case "S": case "s":
                moveDown(newGame);
                break;
            case "D": case "d":
                moveRight(newGame);
                break;
            default:
                System.out.println("Try Again");
        }
    }

    private void moveRight(char[][] newGame) {
        int row = 0;
        int col = 0;
        for(int i = 0; i < newGame.length; i++){
            for(int j = 0; j < newGame[i].length; j++){
                if(newGame[i][j] == this.getHumans()){
                    row = i;
                    col = j;
                }
            }
        }
        if(col + 1 <= newGame.length - 1){
            newGame[row][col + 1] = this.getHumans();
            if(col % 2 == 0){
                newGame[row][col] = ' ';
            }
            else{
                newGame[row][col] = '|';
            }
        }
        else{
            System.out.println("Out of Bounds");
        }
    }

    private void moveDown(char[][] newGame) {
        int row = 0;
        int col = 0;
        for(int i = 0; i < newGame.length; i++){
            for(int j = 0; j < newGame[i].length; j++){
                if(newGame[i][j] == this.getHumans()){
                    row = i;
                    col = j;
                }
            }
        }
        if(row + 1 <= newGame.length - 1){
            newGame[row + 1][col] = this.getHumans();
            if(col % 2 == 0){
                newGame[row][col] = ' ';
            }
            else{
                newGame[row][col] = '|';
            }
        }
        else{
            System.out.println("Out of Bounds");
        }
    }

    private void moveLeft(char[][] newGame) {
        int row = 0;
        int col = 0;
        for(int i = 0; i < newGame.length; i++){
            for(int j = 0; j < newGame[i].length; j++){
                if(newGame[i][j] == this.getHumans()){
                    row = i;
                    col = j;
                }
            }
        }
        if(col - 1 >= 0){
            newGame[row][col - 1] = this.getHumans();
            if(col % 2 == 0){
                newGame[row][col] = ' ';
            }
            else{
                newGame[row][col] = '|';
            }
        }
        else{
            System.out.println("Out of Bounds");
        }
    }

    private void moveUp(char[][] newGame) {
        int row = 0;
        int col = 0;
        for(int i = 0; i < newGame.length; i++){
            for(int j = 0; j < newGame[i].length; j++){
                if(newGame[i][j] == this.getHumans()){
                    row = i;
                    col = j;
                }
            }
        }
        if(row - 1 >= 0){

            newGame[row - 1][col] = this.getHumans();
            if(col % 2 == 0){
                newGame[row][col] = ' ';
            }
            else{
                newGame[row][col] = '|';
            }
        }
        else{
            System.out.println("Out of Bounds");
        }
    }
}

class Goblins {

    char goblinIcon = '\u8475';
    int goblinStrength = 10;
    int goblinHealth = 10;

    public char getGoblins(){
        return goblinIcon;
    }

    public void move(char[][] newGame) {

        Random rand = new Random();
        int movement = rand.nextInt(5);
        switch (movement){
            case 0: case 1:
                move_goblin_left(newGame);
                break;
            case 2:
                move_goblin_right(newGame);
                break;
            case 3:
                move_goblin_up(newGame);
                break;
            case 4: case 5:
                move_goblin_down(newGame);
                break;
            default:
        }
    }

    private void move_goblin_up(char[][] newGame) {
        int row = 0;
        int col = 0;
        int humanRow = 0;
        int humanCol = 0;
        for(int i = 0; i < newGame.length; i++){
            for(int j = 0; j < newGame[i].length; j++){
                if(newGame[i][j] == this.getGoblins()){
                    row = i;
                    col = j;
                }
                if (newGame[i][j] == new Human().getHumans()){
                    humanRow = i;
                    humanCol = j;
                }
            }
        }
        if (row - 1 >= 0){
            if(newGame[row - 1][col] == newGame[humanRow][humanCol]){
                System.out.println("Do Battle!");
            }
            else {
                newGame[row - 1][col] = this.getGoblins();
                if (col % 2 == 0) {
                    newGame[row][col] = ' ';
                } else {
                    newGame[row][col] = '|';
                }
            }
        }
    }

    private void move_goblin_right(char[][] newGame) {
        int row = 0;
        int col = 0;
        int humanRow = 0;
        int humanCol = 0;
        for(int i = 0; i < newGame.length; i++){
            for(int j = 0; j < newGame[i].length; j++){
                if(newGame[i][j] == this.getGoblins()){
                    row = i;
                    col = j;
                }
                if (newGame[i][j] == new Human().getHumans()){
                    humanRow = i;
                    humanCol = j;
                }
            }
        }
        if (col + 1 <= newGame.length){
            if(newGame[row][col + 1] == newGame[humanRow][humanCol]){
                System.out.println("Do Battle!");
            }
            else {
                newGame[row][col + 1] = this.getGoblins();
                if (col % 2 == 0) {
                    newGame[row][col] = ' ';
                } else {
                    newGame[row][col] = '|';
                }
            }
        }
    }

    private void move_goblin_left(char[][] newGame) {
        int row = 0;
        int col = 0;
        int humanRow = 0;
        int humanCol = 0;
        for(int i = 0; i < newGame.length; i++){
            for(int j = 0; j < newGame[i].length; j++){
                if(newGame[i][j] == this.getGoblins()){
                    row = i;
                    col = j;
                }
                if (newGame[i][j] == new Human().getHumans()){
                    humanRow = i;
                    humanCol = j;
                }
            }
        }
        if (col - 1 >= 0){
            if(newGame[row][col - 1] == newGame[humanRow][humanCol]){
                System.out.println("Do Battle!");
            }
            else {
                newGame[row][col - 1] = this.getGoblins();
                if (col % 2 == 0) {
                    newGame[row][col] = ' ';
                } else {
                    newGame[row][col] = '|';
                }
            }
        }
    }

    private void move_goblin_down(char[][] newGame) {
        int row = 0;
        int col = 0;
        int humanRow = 0;
        int humanCol = 0;
        for(int i = 0; i < newGame.length; i++){
            for(int j = 0; j < newGame[i].length; j++){
                if(newGame[i][j] == this.getGoblins()){
                    row = i;
                    col = j;
                }
                if (newGame[i][j] == new Human().getHumans()){
                    humanRow = i;
                    humanCol = j;
                }
            }
        }
        if (row + 1 <= newGame.length){
            if(newGame[row + 1][col] == newGame[humanRow][humanCol]){
                System.out.println("Do Battle!");
            }
            else {
                newGame[row + 1][col] = this.getGoblins();
                if (col % 2 == 0) {
                    newGame[row][col] = ' ';
                } else {
                    newGame[row][col] = '|';
                }
            }
        }
    }
}

class Beings {
    public void beings(){

    }
}
